function each(arr,cb){

    if(!arr || !cb){
        return;
    }
    for(let i =0;i<arr.length;i++){
        cb(arr[i],i);
    }
}
module.exports = each