const each = require('../each.js')

//1.
let arr = [1, 2, 3, 4, 5, 5]
let cb = function(x,i){console.log(x)}
each(arr,cb)

//2.arr not an array
arr = {'val1':2,'val2':8}
cb = function(x,i){console.log(x)}
each(arr,cb)

//3.empty array
arr = []
cb = function(x,i){console.log(x)}
each(arr,cb)

//4.cb =null
arr = [1, 2, 3, 4, 5, 5];
cb = null
each(arr,cb)

//5.arr=null/undefined
arr=undefined
cb = function(x,i){console.log(x)}
each(arr,cb)



