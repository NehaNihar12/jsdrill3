const map = require('../map.js')

//1.
let arr = [1,2,3,4,5,5]
let cb = function(x,i){return x*2}
let result = map(arr,cb)
console.log(result)

//2. function that dosent return true or false
arr = [1,2,3,4,5,5]
cb = function(x,i){ return x+=2}
result = map(arr,cb)
console.log(result)

//3. Empty array
arr = []
cb = function(x,i){return true}
result = map(arr,cb)
console.log(result)

//4. Not an array
arr = 'abc'
cb = function(x,i){return true}
result = map(arr,cb)
console.log(result)

//5. Not a function
arr = [1,2,3,4,5,5]
cb = 'abc'
result = map(arr,cb)
console.log(result)