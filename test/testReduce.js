const reduce = require('../reduce.js')

//1.
let arr = [1,2,3,4,5,5]
let initialVal = 0
let cb = function(startingValue,x){return startingValue+x}
let result = reduce(arr,cb,initialVal)
console.log("1. "+result)

//2. Empty array
arr = []
initialVal = 0
cb = function(startingValue,x){return startingValue+x}
result = reduce(arr,cb,initialVal)
console.log("2. "+result)

//3.starting value =null
arr=[1,2,3,4,5,5]
cb = function(startingValue,x,arr){return startingValue+x}
result = reduce(arr,cb)
console.log("3. "+result)

 //4.arr = null value
 cb = function(startingValue,x){return startingValue+x}
 result = reduce(cb)
 console.log("4. "+result)
 
 //5.cb = null value
 result = reduce(arr)
 console.log("5. "+result)
 